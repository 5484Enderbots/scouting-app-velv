#!/usr/bin/env python3
import csv
import sys
import glob
import os.path

header = False
teamcount = 0
compdata = []
def highscore(x, thresh):
    a = []
    for n in x:
        if int(n) >= thresh:
            a.append("*" + n + "*")
        else:
            a.append(n)
    return a

cap = {
	0: "",
	10: "pickup",
	20: "raised",
	40: "capped",
}
def capmap(x):
    return [ cap[int(a)] for a in x ]

def getnumquals(compname):
    if compname[-5:] == 'cmpd0':
        # event elimination bracket for division winners - no qualifiers
        return 0
    elif compname[:3] in ['wsr', 'esr', 'ssr', 'nsr']:
        # super regional tournaments have 9 qualifiers per team
        return 9
    else:
        # all other tournaments have 5 qualifiers per team - if any less they are too small for playoff rounds
        return 5
class Team(object):

    def __init__(self, number, compname):
        global teamcount
        self.isalliancecapt = False
        self.number = number
        self.compname = compname
        self.autoPoints = []
        self.teleopPoints = []
        self.penaltyPoints = []
        self.endgamePoints = []
        self.teammates = []
        self.allianceColors = []
        self.totalPoints = []
        teamcount += 1

    # note that penalty is penalties against the team's alliance, not penalties against its opponents
    def add(self, alliance, total, auto, teleop, endgame, penalty, teammate1, teammate2, first=False):

        self.totalPoints.append(total)
        self.autoPoints.append(auto)  
        self.teleopPoints.append(teleop)  
        self.endgamePoints.append(endgame)  
        self.penaltyPoints.append(penalty)
        self.teammates.append((teammate1, teammate2))  
        self.allianceColors.append(alliance)
        if len(self.totalPoints) > getnumquals(self.compname) and first:
            self.isalliancecapt = True

    def __str__(self):
        nl = "\n" + self.number + ":\t" if header else "\n"
        ind = "    "
        a = "Team " + str(self.number) + " at " + self.compname + (" (alliance capt.): " if self.isalliancecapt else ": ") + nl + nl
        a += (ind + "color:  \t" + ("\t".join(self.allianceColors)) + nl)
        a += (ind + "auto:   \t" + ("\t".join(highscore(self.autoPoints, 75))) + nl)
        a += (ind + "teleop: \t" + ("\t".join(highscore(self.teleopPoints, 85))) + nl)
        a += (ind + "endgame:\t" + ("\t".join(capmap(self.endgamePoints))) + nl + nl)
        a += (ind + "penalty:\t" + ("\t".join(self.penaltyPoints)) + nl)
        a += (ind + "total:  \t" + ("\t".join(self.totalPoints)) + nl + nl)
        a += (ind + "ally1:  \t" + ("\t".join([partner for partner, _ in self.teammates]) + nl))
        a += (ind + "ally2:  \t")
        for _, partner in self.teammates:
            if int(partner) == 0:
                a += " \t"
            else:
                a += partner + "\t"
        a += "\n"

        return a
    
    def __lt__(self, other):
        return int(self.number) < int(other.number)

class Competition(object):

    def getTeam(self, number):
        if number not in self.teams.keys():
            self.teams[number] = Team(number, self.name)
            return self.teams[number]
        else:
            return self.teams[number]

    def __init__(self, filename):
        self.teams = {}
        self.name = os.path.basename(filename)[9:-24]
        with open(filename) as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                if row[1] == "Match":
                    continue
                red1 = self.getTeam(row[3])
                red2 = self.getTeam(row[4])
                red3 = self.getTeam(row[5])
                # color, auto, tele, end, bluepenalties, partners 
                red1.add("red", row[9], row[10], row[12], row[13], row[20], row[4], row[5], first=True)
                red2.add("red", row[9], row[10], row[12], row[13], row[20], row[3], row[5])
                if int(row[5]) != 0:
                    red3.add("red", row[9], row[10], row[12], row[13], row[20], row[3], row[4])

                blue1 = self.getTeam(row[6])
                blue2 = self.getTeam(row[7])
                blue3 = self.getTeam(row[8])
                
                # color, auto, tele, end, redpenalties, partners 
                blue1.add("blue", row[15], row[16], row[18], row[19], row[14], row[7], row[8], first=True)
                blue2.add("blue", row[15], row[16], row[18], row[19], row[14], row[6], row[8])
                if int(row[8]) != 0:
                    blue3.add("blue", row[15], row[16], row[18], row[19], row[14], row[6], row[7])
            
def getTeamMatches(number, name=""):
    res = "All tournaments for team {} {}:\n\n".format(number, name)
    for comp in compdata:
        if str(number) not in comp.teams.keys():
            continue
        res += str(comp.teams[str(number)]) + "\n"
    return res

def getTeam(number, name=""):
    res = []
    for comp in compdata:
        if str(number) not in comp.teams.keys():
            continue
        res.append(comp.teams[str(number)])
    return res

def parseTeams(argv):
    teams = []
    for arg in argv:
        pair = [ a.strip() for a in arg.split(":") ]
        if len(pair) == 1:
            pair.append("")
        teams.append(pair)
    return teams
         
if os.path.isdir("./ftc-data"):
    comps = glob.glob("ftc-data/events/1617velv/**/*MatchResultsDetails.csv")
    for comp in comps:
        compdata.append(Competition(comp))

if __name__ == "__main__":
    print("Loaded {} competitions with {} team participations".format(len(compdata), teamcount))
    if sys.argv[1] == "print":
        for team in parseTeams(sys.argv[2:]):
            print(getTeamMatches(team[0], team[1]))
    elif sys.argv[1] == "write":
        teams = ""
        with open(sys.argv[2]) as teamfile:
            teams = teamfile.read().splitlines()
        for team in parseTeams(teams):
            with open(os.path.join(sys.argv[3], team[0] + " " + team[1] + ".txt"), "w") as f:
                f.write(getTeamMatches(compdata, team[0], team[1]))

                
    else:
        print("usage: " + sys.argv[0] + "[print|write] teamno:teamname ...")
