import matchparse
class Team(object):
    def __init__(self, number, name):
        self.matches = []
        self.number = number
        self.name = name
        self.notes = ""

    def add_match(self, form):
        a = matchparse.MatchPerformance(self.number)
        a.from_dict(form)
        self.matches.append(a)
    def delete_match(self, number):
        self.matches.pop(number) 
    def from_sql(self, number, cursor):
        self.number = number
        items = cursor.execute(
"""SELECT team,
match_number,
alliance_partner,
auto_close_beacon,
auto_far_beacon,
auto_cap_ball,
auto_center_particles,
auto_corner_particles,
auto_end,
teleop_center_particles,
teleop_corner_particles,
teleop_beacon,
teleop_cap_ball
 FROM matches WHERE team = ?""".replace("\n",""), number)
        for row in items:
            a = MatchPerformance(number)
            for i in range(len(a.ATTRS)):
                setattr(a, a.ATTRS[i], row[i])
            self.matches.append(a)
