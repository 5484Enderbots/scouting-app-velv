import collections
import traceback
class MatchPerformance(object):
    AUTO_END_STATES = ['none', 'corner', 'center']
    TELEOP_CAP_BALL_STATES = ['none', 'tried', 'dropped', 'pickup', 'raised', 'capped']
    ATTRS = ['team', 'alliance_partner', 'auto_close_beacon', 'auto_far_beacon', 'auto_cap_ball', 'auto_center_particles',
            'auto_corner_particles', 'auto_end', 'teleop_center_particles', 'teleop_corner_particles', 'teleop_beacon', 
            'teleop_cap_ball']
    def __init__(self, team):
        self.team = int(team)
        self.alliance_partner = 0
        self.auto_close_beacon = False
        self.auto_far_beacon = False
        self.auto_cap_ball = False
        self.auto_center_particles = 0
        self.auto_corner_particles = 0
        self.auto_end = self.AUTO_END_STATES[0]
        self.teleop_center_particles = 0
        self.teleop_corner_particles = 0
        self.teleop_beacon = 0
        self.teleop_cap_ball = self.TELEOP_CAP_BALL_STATES[0]

    def from_dict(self, form):
        if "auto_close_beacon" in form.keys():
            self.auto_close_beacon = True

        if "auto_far_beacon" in form.keys():
            self.auto_far_beacon = True

        if "auto_cap_ball" in form.keys():
            self.auto_cap_ball = True

        try:
            self.alliance_partner = int(form['alliance_partner'])
            self.auto_center_particles = int(form['auto_center_particles'])
            self.auto_corner_particles = int(form['auto_corner_particles'])
        except Exception:
            traceback.print_exc()
            return "invalid number of autonomous particles"
        
        auto_end = form['auto_end']

        if auto_end not in self.AUTO_END_STATES:
            return "invalid autonomous end state"
        
        self.auto_end = auto_end

        try:
            self.teleop_center_particles = int(form['teleop_center_particles'])
            self.teleop_corner_particles = int(form['teleop_corner_particles'])
            self.teleop_beacon = int(form['teleop_beacon'])
        except Exception:
            traceback.print_exc()
            return "invalid teleop values"

        
        teleop_cap = form['teleop_cap_ball']

        if teleop_cap not in self.TELEOP_CAP_BALL_STATES:
            traceback.print_exc()
            return "invalid cap ball state"
        
        self.teleop_cap_ball = teleop_cap

        return None
    
    def to_dict(self):
        ret = collections.OrderedDict()
        for a in self.ATTRS:
            ret[a] = getattr(self, a)

        return ret

