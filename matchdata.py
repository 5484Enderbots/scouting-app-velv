#!/usr/bin/env python3
import csv
import sys
import glob
import os.path
import matchdata_det
import matchdata_score

header = False
teamcount = 0

auto_park = {
        "0": "",
        "1": "center",
        "2": "centerF",
        "3": "corner",
        "4": "cornerF",
}

cap_ball = {
        "0": "",
        "1": "pickup",
        "2": "raised",
        "3": "capped",
}

def autonum(n):
    #if int(n) == 0: return ""
    if int(n) >= 2: return "*" + n + "*"
    return n

def shoot(n):
    if int(n) >= 10: return "*" + n + "*"
    return n
def teleopbeacon(n):
    if int(n) == 4: return "*" + n + "*"
    return n

def autocap(n):
    if n == 'TRUE':
        return 'y'
    else:
        return ''

def getnumquals(compname):
    if compname[-5:] == 'cmpd0':
        # event elimination bracket for division winners - no qualifiers
        return 0
    elif compname[:3] in ['wsr', 'esr', 'ssr', 'nsr']:
        # super regional tournaments have 9 qualifiers per team
        return 9
    else:
        # all other tournaments have 5 qualifiers per team - if any less they are too small for playoff rounds
        return 5

class Team(object):

    def __init__(self, number, compname):
        global teamcount
        self.number = number
        self.compname = compname
        self.teammates = []
        self.allianceColors = []
        self.perfs = []
        self.slots = []
        self.isalliancecapt = False
        teamcount += 1

    # note that penalty is penalties against the team's alliance, not penalties against its opponents
    def add(self, color, slot, perf, teammate1, teammate2):
        self.perfs.append(perf)
        self.teammates.append((teammate1, teammate2))  
        self.allianceColors.append(color)
        self.slots.append(slot)
        if len(self.slots) > getnumquals(self.compname) and slot == 0:
            self.isalliancecapt = True

    def __str__(self):
        ind = "    "
        nl = "\n" + self.number + ":\t" if header else "\n"
        a = "Team " + str(self.number) + " at " + self.compname + (" (alliance capt.): " if self.isalliancecapt else ": ") + nl
        a += (ind + "color:        \t" + ("\t".join(self.allianceColors)) + nl)
        a += (ind + "auto beacon:  \t" + ("\t".join([ autonum(i[0]) for i in self.perfs ])) + nl)
        a += (ind + "auto shoot:   \t" + ("\t".join([ autonum(i[2]) for i in self.perfs ])) + nl)
        a += (ind + "auto cap ball:\t" + ("\t".join([ autocap(i[1]) for i in self.perfs ])) + nl)
        a += (ind + "auto park:    \t" + ("\t".join(
            [ auto_park[self.perfs[i][4 + self.slots[i]]] for i in range(len(self.slots)) ]
        )) + nl + nl)
        a += (ind + "teleop shoot: \t" + ("\t".join([ shoot(i[7]) for i in self.perfs ])) + nl)
        a += (ind + "teleop beacon:\t" + ("\t".join([ teleopbeacon(i[6]) for i in self.perfs ])) + nl)
        a += (ind + "cap ball:     \t" + ("\t".join([ cap_ball[i[9]] for i in self.perfs ])) + nl + nl)
        # TODO: add penalty?
        a += (ind + "ally1:        \t" + ("\t".join([partner for partner, _ in self.teammates]) + nl))
        a += (ind + "ally2:        \t")
        for _, partner in self.teammates:
            if int(partner) == 0:
                a += " \t"
            else:
                a += partner + "\t"
        a += "\n"

        return a
    
    def __lt__(self, other):
        return int(self.number) < int(other.number)

class Competition(object):

    def getTeam(self, number):
        if number not in self.teams.keys():
            self.teams[number] = Team(number, self.name)
            return self.teams[number]
        else:
            return self.teams[number]

    def __init__(self, filename):
        self.teams = {}
        self.name = os.path.basename(filename)[9:-20]
        with open(filename) as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                if row[0] == "TournamentMatchCode":
                    continue
                red1 = self.getTeam(row[4])
                red2 = self.getTeam(row[5])
                red3 = self.getTeam(row[6])
                # color, auto, tele, end, bluepenalties, partners 
                red1.add("red", 0, row[29:], row[5], row[6])
                red2.add("red", 1, row[29:], row[4], row[6])
                if int(row[6]) != 0:
                    red3.add("red", 1, row[29:], row[4], row[5])

                blue1 = self.getTeam(row[7])
                blue2 = self.getTeam(row[8])
                blue3 = self.getTeam(row[9])
                
                # color, auto, tele, end, redpenalties, partners 
                blue1.add("blue", 0, row[43:], row[8], row[9])
                blue2.add("blue", 1, row[43:], row[7], row[9])
                if int(row[9]) != 0:
                    blue3.add("blue", 1, row[43:], row[7], row[8])

compdata = []
def getTeamMatches(number, name=""):
    det = { m.compname: m for m in matchdata_det.getTeam(number) }
    score = { m.compname: m for m in matchdata_score.getTeam(number) }

    names = []
    res = "All tournaments for team {} {}:\n\n".format(number, name)
    # first try the matches with a high level of detail
    for comp in compdata:
        if str(number) not in comp.teams.keys():
            continue
        res += str(comp.teams[str(number)]) + "\n"

        names.append(comp.name)

    # then try the matches with only auto/teleop/endgame aggregates
    for det_name in det.keys():
        if det_name not in names:
            res += str(det[det_name]) + "\n"
            names.append(det_name)

    # then try the matches with only final scores available :/
    for score_name in score.keys():
        if score_name not in names:
            res += str(score[score_name]) + "\n"
            
    return res

def parseTeams(argv):
    teams = []
    for arg in argv:
        print(arg)
        try: 
            if arg[0] == '#':
                continue
        except IndexError:
            continue
        pair = [ a.strip() for a in arg.split(":") ]
        if len(pair) == 1:
            pair.append("")
        teams.append(pair)
    return teams
         

comps = glob.glob("ftc-data/events/1617velv/**/*MatchResultsRaw.csv")
for comp in comps:
    compdata.append(Competition(comp))

if __name__ == "__main__":
    print("Loaded {} competitions with {} team participations".format(len(compdata), teamcount))
    if sys.argv[1] == "print":
        for team in parseTeams(sys.argv[2:]):
            print(getTeamMatches(team[0], team[1]))
    elif sys.argv[1] == "write":
        teams = ""
        with open(sys.argv[2]) as teamfile:
            teams = teamfile.read().splitlines()
        for team in parseTeams(teams):
            with open(os.path.join(sys.argv[3], team[0] + " " + team[1] + ".txt"), "w") as f:
                f.write(getTeamMatches(team[0], team[1]))

                
    else:
        print("usage: " + sys.argv[0] + "[print|write] teamno:teamname ...")
