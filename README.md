#5484 Enderbots Scouting App

###How to install: (may not be relevant depending on how this is distributed)

1. install python 3 and git
2. in a virtualenv or something else install Flask using `pip install flask`
3. `git clone https://github.com/cheer4ftc/ftc-data.git` for  match data - to update: `cd ftc-data` then `git pull`


###to run:

`python3 main.py` or other supplied shortcut in your distribution

point your browser to http://127.0.0.1:5484 (cool port number, right?)


###using:
There's a team adding and removal interface linked in the top right. No, the file select doesn't work (yet)

On each team stats page, there's inputted matches, and notes on the robot or team as a whole.

To update the notes, press Update Notes after editing the text box.

To add a match, press Add match and check the boxes according to how a match went according to your scouts.

To delete a match, go to the stats page of the team and press Delete on the match row.


inb4 "should've used bootstrap" or "pickle files aren't real databases"
