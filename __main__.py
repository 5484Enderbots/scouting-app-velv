#!/usr/bin/env python3

import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "deps"))

from flask import Flask, render_template, request, redirect, url_for, send_from_directory
import pprint
import matchparse
import teams
import collections
import matchdata
import pickle
import json
app = Flask(__name__, static_url_path="/static")


def parse_teams(path, teams_list):
    with open(path) as f:
        for line in f:
            l = line.strip()
            if len(l) <= 0:
                continue
            if l[0] == '#':
                continue
            pair = l.split(':')
            if len(pair) == 1:
                pair.append("Team " + pair[0])
            teams_list.append((pair[0], ':'.join(pair[1:]))) 

def save_data(path, teams_obj):
    with open(path, 'wb') as f:
        pickle.dump(teams_obj, f)

database_file = "./db.pickle"
teams_list = []
teams_matchdata = {}


parse_teams('./teams', teams_list)
teams_list.sort(key=lambda x: int(x[0]))

teams_obj = None
try:
    with open(database_file, 'rb') as f:
        teams_obj = pickle.load(f)
    print("Loaded database file.")
except (FileNotFoundError, pickle.PickleError) as e:
    print("no database found, creating a new one")
    teams_obj = None

if teams_obj is None:    
    teams_obj = collections.OrderedDict()
    for num, name in teams_list:
        teams_obj[num] = teams.Team(num, name)

@app.route('/')
def home():
    return redirect('/teams')

@app.route('/teams')
def all_team_stats():
    return render_template('all_team_stats.html', teams_data=teams_obj)

@app.route('/teamsedit', methods=['GET', 'POST'])
def edit_teams():
    return render_template('input_teams.html', teams_data=teams_obj)


@app.route('/teamsedit/update', methods=['POST'])
def update_teams():
    global teams_obj
    data = json.loads(request.form['data']) 
    for teamno, teamname in data.items():
        assert int(teamno)
        assert teamno.strip() == teamno
        if teamno not in teams_obj.keys():
           teams_obj[teamno] = teams.Team(teamno, teamname)
    
    delete_queue = []
    for teamno in teams_obj.keys():
        if teamno not in data.keys():
            delete_queue.append(teamno)
    for e in delete_queue:
        del teams_obj[e]

    teams_obj = collections.OrderedDict(sorted(teams_obj.items(), key=lambda x: int(x[0])))
    save_data(database_file, teams_obj)

    return redirect('/teamsedit')



@app.route('/teams/<number>')
def team_stats(number=None):
    match_data = matchdata.getTeamMatches(number, teams_obj[number].name)

    matches = [ m.to_dict() for m in teams_obj[number].matches ]
    return render_template('team_stats.html', number=number, teams_data=teams_obj, teamname=teams_obj[number].name, notes_textarea=teams_obj[number].notes, matches=matches, match_data=match_data)

@app.route('/teams/<number>/match')
def add_match(number=None):
    return render_template('input_match.html', number=number, teams_data=teams_obj, notes_textarea=teams_obj[number].notes)

@app.route('/teams/<number>/delete_match', methods=['POST'])
def delete_match(number):
    # TODO: delete by number,
    teams_obj[number].delete_match(int(request.form['match_number']))

    save_data(database_file, teams_obj)
    return redirect('/teams/' + number)
@app.route('/teams/<number>/update', methods=['POST'])
def append_match(number=None):
    global teams_obj
    teams_obj[number].add_match(request.form)
    teams_obj[number].notes = request.form['notes_content']

    save_data(database_file, teams_obj)
    return redirect('/teams/' + number)

@app.route('/teams/<number>/update_notes', methods=['POST'])
def update_notes(number):
    global teams_obj
    teams_obj[number].notes = request.form['notes_content']

    save_data(database_file, teams_obj)
    return redirect('/teams/' + number)

if __name__ == "__main__":
    app.run(port=5484)
